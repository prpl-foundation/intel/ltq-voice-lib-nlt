#ifndef _LIB_TAPI_NLT_VERSION_H_
#define _LIB_TAPI_NLT_VERSION_H_
/******************************************************************************

                         Copyright (c) 2012, 2015, 2016
                        Lantiq Beteiligungs-GmbH & Co.KG
                             http://www.lantiq.com

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

*******************************************************************************/

/**
    \file  lib_tapi_nlt_version.h
    \brief Holds TAPI Line Testing library version number.
*/

/** library version, major number */
#define LIB_TAPI_NLT_VER_MAJOR 0
/** library version, minor number */
#define LIB_TAPI_NLT_VER_MINOR 6
/** library version, build number */
#define LIB_TAPI_NLT_VER_STEP  0
/** library version, package type */
#define LIB_TAPI_NLT_VER_TYPE  5

#endif /*_LIB_TAPI_NLT_VERSION_H_ */

